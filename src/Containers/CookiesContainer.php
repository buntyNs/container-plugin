<?php

namespace CookieLow\Containers;

use Plenty\Plugin\Templates\Twig;
    
class CookiesContainer
{
    public function call(Twig $twig):string
    {
        return $twig->render('CookieLow::content.Banner');
    }
}